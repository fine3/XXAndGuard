XXAndGuard
============================

### 简介

![](https://jitpack.io/v/com.gitlab.fine3/XXAndGuard.svg)

这是个安卓编译Gradle插件合集，用于自定义安卓app编译过程。

### 使用方法

```groovy
buildscript {
    repositories {
        maven { url 'https://jitpack.io' }
    }
    dependencies {
        classpath 'com.gitlab.fine3:XXAndGuard:+'
    }
}

allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}
```

### 插件功能

#### 1. 垃圾代码生成插件

为了防止源文件静态查重，我们可以使用该插件随机插入大量源代码，资源文件。 插件功能：

* 在指定包名下随机生成多个新包名，在每个包名下随机生成垃圾java文件
* 在每个包名下生成垃圾Activity代码，垃圾Activity自动注册到AndroidManifest，防止shrink清理
* 为垃圾Activity生成对应的布局文件，并在Activity中生成setContentView方法与之关联，防止shrink清理
* 为每个布局文件生成相关联的随机dimens，strings，drawables，colors,随机资源最终都被引用，防止shrink清理
* drawables资源颜色随机，编译后生成的png图片hash值各不相同
* 生成代码目录：/build/generated/source/junk/

```groovy
apply plugin: 'com.fine.junk'

junkCode {
    basePackages = [    //指定包名集合
                        'com.demo',
    ]
    packageCount = 2 //生成包数量
    activityCountPerPackage = 2 //每个包下生成Activity类数量
    excludeActivityJavaFile = false //是否排除生成Activity的Java文件
    otherCountPerPackage = 2 //每个包下生成其它类的数量
    methodCountPerClass = 2 //每个类下生成方法数量
    resPrefix = 'jc_' //生成的layout、drawable、string等资源名前缀
    imageCountLimit = 100 //图片总数限制，到达限制后会重用图片(图片多了会影响包大小)
    debugEnable = false //debug模式下是否开启
}
```

#### 2. java常量字符串自动加密插件

为了防止应用被反编译并静态检查代码中的字符串常量，可以使用该插件加密字符串常量。插件功能：

* 实现指定包名下的class文件中常量字符串编译时加密，运行时调用解密方法解密。
* 程序执行效果与使用插件之前无异
* 如果静态反编译dex，只能得到密文
* 每次编译密文也会不同，因为处理每个字符串时，会使用随机的字符串
* 加密日志文件：build\outputs\mapping\XXX\string-encode.txt

```groovy
apply plugin: 'com.fine.string-encode'

stringEncode {
    basePackages = [    //指定包名集合
                        'com.demo'
    ]
    debugEnable = true  //调试模式是否打开
    printLog = true //是否打印日志
}
```

加密前后静态反编译dex效果对比：

```java
//加密前
String contentType="data:image/jpeg;base64,";
public static final String OS="os";

//加密后
        String contentType=De.de("KysEyRHNv2pAZ/93mqup2fbr+TEXsn30a3HUO+JwQQ==");
public static final String OS=De.de("gB6E0KFDahPqRwo=");
```

解密算法使用的方法会自动引入，无需手动引用：

```java
package com.hzy.xxteaw;

public class De {
    public static String de(String txt);
}
```

#### 3. 混淆字典随机生成插件

java字节码编译过程中虽然打开混淆开关，但不使用字典或者使用静态字典， 则每次编译生成的混淆结果都一样，而我们希望每次混淆的结果都不同。 该插件用于在编译过程中随机动态生成(乱序)
并应用混淆字典，从而达到这个目的。

* 插件从指定源字典中读取字典列表并打乱顺序重新生成一个字典，混淆时自动应用该字典
* 如果未指定源字典，插件提供一个默认字典，由4000多个乱码符号组成
* 该任务被ProguardConfigurableTask所依赖，只有打开系统混淆开关，该插件才会生效
* 安卓R8混淆前会合并所有类库以及该项目的混淆配置文件，因此该插件只会影响字典，并不会影响其他配置
* 适用版本：1.1.2+

```groovy
apply plugin: 'com.fine.random-dict'

randomDict {
    printLog = false //是否打印日志，默认为false
    srcDictName = 'dictionary-src.txt'  //源字典，最终应用的乱序字典源于该字典。不指定则使用默认字典
}
```

使用该插件生效需要打开安卓gradle插件的混淆开关，否则无效。例如：

```groovy
release {
    zipAlignEnabled true
    minifyEnabled true
    proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
    signingConfig signingConfigs.release
}
```

有了动态字典，原来项目混淆文件中的静态字典配置可以无需了：

* ~~-obfuscationdictionary proguard-dictionary.txt~~
* ~~-classobfuscationdictionary proguard-dictionary.txt~~
* ~~-packageobfuscationdictionary proguard-dictionary.txt~~

#### 4. APK图片重编码/资源名称混淆插件(仅对apk格式生效)

为防止应用被反编译，静态检查并关联其中的图片资源文件，可以使用该插件在编译时对所有图片资源进行重新编码，以改变图片资源文件哈希值：

* 仅对apk格式生效，若要处理aab格式，请使用下一个插件
* 图片扫描目录：资源文件res目录下(源文件和lib模块中包含的图片都会处理)
* 同一张图片源文件，每次编译后生成的应用包中对应的图片哈希值都不一样
* 目前只处理png格式图片，.9.png格式不会处理
* 图片处理日志文件：build\outputs\mapping\XXX\rehash-images.txt
* 修改图片hash使用jdk中的编码方法，生成的新图可能比源图片文件大
* 加入了pngquant可以对png进行压缩
* 对所有资源文件名称随机命名,每次都不一样
* 适用版本：1.1.1+

```groovy
apply plugin: 'com.fine.rehash-imgs'

rehashImages {
    rehashIgnores = [ //不重新编码该前缀的图片
                       'abc_'
    ]
    debugEnable = false //是否对debug包生效
    enableNameGuard = false //是否修改资源文件名
    enablePngMinify = false //是否使用pngquant对png进行压缩
    printLog = false //是否打印日志
}
```

#### 5. AAB图片重编码/资源名称混淆插件(仅对AAB格式生效)

为防止应用被反编译，静态检查并关联其中的图片资源文件，可以使用该插件在编译时对所有图片资源进行重新编码，以改变图片资源文件哈希值：

* 跟com.fine.rehash-imgs插件处理是类似的，区别是该插件仅对aab格式打包生效，因为打包apk并不会执行相关任务
* 任务依赖：打包AAB的过程中，有一个bundle${variantName}Resources任务，这个任务会调用aapt合并并打包资源，生成.ap_文件
* 插件工作流程：解压ap_文件->修改PNG->解析resources.pb文件->资源文件改名同时同步到pb->压缩png->重新构建pb文件->重打包ap_文件
* 该插件只是在正常打包过程中修改了中间文件资源包，如果文件不出错，则不会影响后续的打包，签名等流程
* 适用版本1.1.6+

```groovy
apply plugin: 'com.fine.rehash-aab'

rehashAab {
    rehashIgnores = [ //不重新编码该前缀的图片
                       'abc_'
    ]
    debugEnable = false //是否对debug包生效
    enableNameGuard = false //是否修改资源文件名
    enablePngMinify = false //是否使用pngquant对png进行压缩
    printLog = false //是否打印日志
}
```

#### 6. AAB清除无用资源防止被关联
为防止meta信息关联，使用该插件删除BUNDLE-METADATA

* 该步骤在签名之前进行，所以不会影响后续签名流程
* BUNDLE-METADATA中包含很多敏感信息：混淆map，c++符号表，依赖类库信息等，可以删掉
* 目前meta文件删除后，aab文件在本地可以正常流出apk，不知道GP会不会允许该操作
* 适用版本1.1.7+

```groovy
apply plugin: 'com.fine.clean-aab'
```