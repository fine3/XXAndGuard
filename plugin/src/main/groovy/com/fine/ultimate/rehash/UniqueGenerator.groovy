package com.fine.ultimate.rehash

class UniqueGenerator {

    private static String START_SEED = "abcdefghijklmnopqrstuvwxyz"
    private static String NAME_SEED = "abcdefghijklmnopqrstuvwxyz_0123456789"
    private static String[] WRONG_NAMES = ['con', 'prn', 'aux', 'nul']
    private HashSet<String> mNameSet = new LinkedHashSet<>()
    private LinkedList<String> mNameList = new LinkedList<>()

    String fetch() {
        return mNameList.remove()
    }

    void generateNames(int size) {
        mNameSet.clear()
        generateMore(size)
        mNameList = new LinkedList<>(mNameSet)
        Collections.shuffle(mNameList)
    }

    private void generateMore(int size) {
        generateWei(size, 1, 0)
    }

    private void generateWei(int size, int wei, int lastCount) {
        def curCount = 0
        if (wei == 1) {
            for (def ch : START_SEED) {
                if (mNameSet.size() < size) {
                    mNameSet.add(ch.toString())
                    curCount++
                } else {
                    return
                }
            }
        } else {
            def count = mNameSet.size()
            for (def idx : (count - lastCount)..count) {
                for (def ch : NAME_SEED) {
                    def last = mNameSet[idx]
                    if (mNameSet.size() < size) {
                        def gen = last + ch
                        if (wei == 3 && (gen in WRONG_NAMES)) {
                            continue
                        }
                        mNameSet.add(gen)
                        curCount++
                    } else {
                        return
                    }
                }
            }
        }
        generateWei(size, wei + 1, curCount)
    }
}
