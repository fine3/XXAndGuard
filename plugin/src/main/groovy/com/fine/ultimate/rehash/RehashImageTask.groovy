package com.fine.ultimate.rehash

import com.android.build.gradle.api.ApplicationVariant
import org.apache.commons.io.FileUtils
import org.gradle.api.Project

class RehashImageTask {

    Project mProject
    RehashImagesExt mConfig
    ApplicationVariant mVariant
    APFileOperator mFileOp

    RehashImageTask(Project project, ApplicationVariant variant) {
        mProject = project
        mConfig = project.rehashImages as RehashImagesExt
        mVariant = variant
    }

    void start(File apFile) {
        if (mVariant.buildType.debuggable && !mConfig.debugEnable) {
            return
        }
        def tempDir = new File(apFile.parentFile.parent, '_temp/')
        def srcDir = new File(tempDir, 'src')
        def dstDir = new File(tempDir, 'dst')
        def tempAP = new File(tempDir, 'out.ap_')

        mFileOp = new APFileOperator(mProject, mVariant, 'rehash-images')
        mFileOp.unpackAPFile(apFile.path, srcDir.path)
        ResourceProcessor processor = new ResourceProcessor(srcDir, dstDir, mConfig)
        processor.start()
        mFileOp.repackAPFile(dstDir, tempAP.path)
        def apPath = apFile.path
        FileUtils.delete(apFile)
        FileUtils.moveFile(tempAP, new File(apPath))
    }
}