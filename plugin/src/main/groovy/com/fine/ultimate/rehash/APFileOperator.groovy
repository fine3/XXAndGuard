package com.fine.ultimate.rehash

import com.android.build.gradle.api.ApplicationVariant
import com.fine.ultimate.cmd.CommandUtils
import com.sun.imageio.plugins.png.PNGMetadata
import org.apache.commons.io.FileUtils
import org.gradle.api.Project

import javax.imageio.ImageIO
import javax.imageio.ImageWriteParam
import javax.imageio.metadata.IIOMetadataNode
import java.util.zip.ZipEntry
import java.util.zip.ZipFile

class APFileOperator {

    Project mProject
    ApplicationVariant mVariant
    RehashImagesExt mConfig
    static Random mRandom = new Random()
    File mRehashMapFile

    APFileOperator(Project project, ApplicationVariant variant, String logName) {
        mProject = project
        mVariant = variant
        mConfig = project.rehashImages as RehashImagesExt
        mRehashMapFile = new File(mProject.buildDir, "outputs/mapping/${variant.name}/${logName}.txt")
    }

    HashMap<String, Integer> unpackAPFile(String src, String dst) {
        def dstDir = new File(dst)
        if (dstDir.exists()) {
            FileUtils.deleteDirectory(dstDir)
        }
        dstDir.mkdirs()
        mRehashMapFile.parentFile.mkdirs()
        def rehashMapWriter = new OutputStreamWriter(new FileOutputStream(mRehashMapFile), 'UTF-8')
        ZipFile zipFile = new ZipFile(src)
        Enumeration emu = zipFile.entries()
        HashMap<String, Integer> compress = new HashMap<>()
        try {
            while (emu.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) emu.nextElement()
                if (entry.isDirectory()) {
                    new File(dstDir, entry.getName()).mkdirs()
                    continue
                }
                BufferedInputStream bis = new BufferedInputStream(zipFile.getInputStream(entry))
                File file = new File(dst + File.separator + entry.getName())
                File parent = file.getParentFile()
                if (parent != null && (!parent.exists())) {
                    parent.mkdirs()
                }
                String entryName = entry.getName()
                if (entryName.contains("\\")) {
                    entryName = entryName.replace("\\", "/")
                }
                compress.put(entryName, entry.method)
                if (needRehash(entry)) {
                    rehashMapWriter.write("${entryName}\n")
                    writePNGStream2RandomMetaFile(file, bis)
                } else {
                    FileUtils.copyToFile(bis, file)
                }
                bis.close()
            }
        } finally {
            rehashMapWriter.close()
            zipFile.close()
        }
        return compress
    }

    static void writePNGStream2RandomMetaFile(File outFile, InputStream is) {
        FileOutputStream fos = new FileOutputStream(outFile)
        def reader = ImageIO.getImageReadersByFormatName('png').next()
        reader.setInput(ImageIO.createImageInputStream(is))
        def image = reader.readAll(0, null)
        def pngMeta = reader.getImageMetadata(0)
        IIOMetadataNode textEntry = new IIOMetadataNode("tEXtEntry")
        textEntry.setAttribute("keyword", "${mRandom.nextInt()}")
        textEntry.setAttribute("value", "${mRandom.nextInt()}")
        IIOMetadataNode text = new IIOMetadataNode("tEXt")
        text.appendChild(textEntry)
        IIOMetadataNode root = new IIOMetadataNode(PNGMetadata.nativeMetadataFormatName)
        root.appendChild(text)
        pngMeta.mergeTree(PNGMetadata.nativeMetadataFormatName, root)
        def writer = ImageIO.getImageWritersByFormatName("png").next();
        def imgOs = ImageIO.createImageOutputStream(fos)
        writer.setOutput(imgOs)
        ImageWriteParam param = writer.getDefaultWriteParam()
        writer.write(pngMeta, image, param)
        imgOs.flush()
        imgOs.close()
        fos.close()
    }

    private boolean needRehash(ZipEntry entryIn) {
        if (entryIn.directory) {
            return false
        }
        def enName = entryIn.name.toLowerCase()
        if (enName.endsWith('.9.png')) {
            return false
        }
        if (!enName.endsWith('.png')) {
            return false
        }
        enName = enName.substring(enName.lastIndexOf('/') + 1)
        boolean ret = true
        mConfig.rehashIgnores.each {
            if (enName.startsWith(it)) {
                ret = false
                return
            }
        }
        return ret
    }

    static void repackAPFile(File srcDir, String apPath) {
        if (!srcDir.exists()) {
            return
        }
        def cmdFile = new File(srcDir.parent, 'p7zip')
        def srcFiles = "${srcDir.path + File.separator}*"
        CommandUtils.compressWithCommand(cmdFile, apPath, srcFiles, '-mx9')
        CommandUtils.compressWithCommand(cmdFile, apPath, "${srcDir.path + File.separator}resources.arsc", '-mx0')
    }
}
