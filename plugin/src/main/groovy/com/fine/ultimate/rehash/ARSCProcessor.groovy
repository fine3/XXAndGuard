package com.fine.ultimate.rehash

import com.reandroid.lib.arsc.chunk.TableBlock

class ARSCProcessor {

    private final File mSrcDir
    private final File mDstDir
    private final File mResDir
    private final File mARDCFile
    private ResCopier mResCopier

    ARSCProcessor(File srcDir, File dstDir) throws Exception {
        mSrcDir = srcDir
        mDstDir = dstDir
        mResDir = new File(srcDir, "res")
        if (!(mResDir.exists() && mResDir.isDirectory())) {
            throw new Exception("res not exists!")
        }
        mARDCFile = new File(srcDir, "resources.arsc")
        if (!(mARDCFile.exists() && mARDCFile.isFile())) {
            throw new Exception("resources.arsc not exists!")
        }
    }

    void setCopier(ResCopier copier) {
        mResCopier = copier
    }

    void parseARDC() throws Exception {
        def tableBlock = TableBlock.load(mARDCFile)
        def stringPool = tableBlock.getTableStringPool()
        def uniGenerator = new UniqueGenerator()
        uniGenerator.generateNames(stringPool.countStrings())

        mResDir.listFiles().each { sub ->
            if (sub.isDirectory()) {
                def subName = "res/${sub.name}/"
                sub.listFiles().each { srcFile ->
                    def resPath = "${subName}${srcFile.name}"
                    def resExt = getLongExtension(srcFile)
                    def stringGroup = stringPool.get(resPath)
                    def genName = uniGenerator.fetch()
                    stringGroup.each { tabStr ->
                        def destRes = "${subName}${genName}${resExt}"
                        tabStr.set(destRes)
                        def destFile = new File(mDstDir, destRes.replace('/', File.separator))
                        if (!destFile.exists() && mResCopier != null) {
                            mResCopier.onFileNeedCopy(srcFile, destFile)
                        }
                        tabStr.listReferencedEntries().each { eb ->
                            //eb.specString.set(genName)
                        }
                    }
                }
            }
        }

        tableBlock.refresh()
        File arscOutFile = new File(mDstDir, 'resources.arsc')
        tableBlock.writeBytes(arscOutFile)
    }

    private static String getLongExtension(File file) {
        def raw = file.name
        def dot1 = raw.indexOf('.')
        if (dot1 != -1) {
            return raw.substring(dot1)
        }
        return ''
    }
}
