package com.fine.ultimate.rehash

import com.fine.ultimate.cmd.CommandUtils
import org.apache.commons.io.FileUtils

class ResourceProcessor implements ResCopier {

    private final File mSrcDir
    private final File mDstDir
    private final File mPngProcFile
    private RehashImagesExt mConfig

    ResourceProcessor(File srcDir, File dstDir, RehashImagesExt ext) {
        mSrcDir = srcDir
        mDstDir = dstDir
        mConfig = ext
        mPngProcFile = new File(srcDir.parent, 'pngquant')
    }

    void start() {
        if (mDstDir.exists()) {
            FileUtils.deleteDirectory(mDstDir)
        }
        mDstDir.mkdirs()
        if (mConfig.enableNameGuard) {
            ARSCProcessor processor = new ARSCProcessor(mSrcDir, mDstDir)
            processor.setCopier(this)
            processor.parseARDC()
            copyOtherFiles()
        } else {
            FileUtils.copyDirectory(mSrcDir, mDstDir)
        }
    }

    private void copyOtherFiles() throws Exception {
        if (!(mSrcDir.exists() && mSrcDir.isDirectory())) {
            throw new Exception("src path error!!")
        }
        File[] subs = mSrcDir.listFiles()
        if (subs == null) {
            return
        }
        subs.each {
            String name = it.name.toLowerCase()
            if (name != 'resources.arsc' && name != 'res') {
                FileUtils.copyToDirectory(it, mDstDir)
            }
        }
    }

    @Override
    boolean onFileNeedCopy(File source, File dest) {
        def srcName = source.name.toLowerCase()
        if (needMinify(srcName)) {
            dest.parentFile.mkdirs()
            CommandUtils.minifyPNGWithCommand(mPngProcFile, source.path, dest.path)
            if (mConfig.printLog) {
                println "minify png: ${source.name}(${getReadableSize(source)}) -> ${dest.name}(${getReadableSize(dest)})"
            }
        } else {
            FileUtils.copyFile(source, dest, false)
        }
        return true
    }

    private static String getReadableSize(File file) {
        return FileUtils.byteCountToDisplaySize(file.size())
    }

    private boolean needMinify(String srcName) {
        if (!mConfig.enablePngMinify) {
            return false
        }
        if (srcName.endsWith('.png') && !srcName.endsWith('.9.png')) {
            return true
        }
        return false
    }
}
