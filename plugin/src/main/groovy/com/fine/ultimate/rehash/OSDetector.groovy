package com.fine.ultimate.rehash;

import org.apache.tools.ant.taskdefs.condition.Os;

class OSDetector {

    static String getArchExt() {
        if (Os.isFamily(Os.FAMILY_WINDOWS)) {
            return 'win'
        } else if (Os.isFamily(Os.FAMILY_MAC)) {
            return "mac"
        } else {
            return "linux"
        }
    }
}
