package com.fine.ultimate.rehash

class ArscDemoApp {

    static void main(String[] args) {
        parseArscDemo()
    }

    private static parseArscDemo() {
        File srcDir = new File('repo', 'temp')
        File dstDir = new File('repo', 'resOut')
        def ext = new RehashImagesExt()
        ext.enableNameGuard = true
        ext.enablePngMinify = true
        ResourceProcessor processor = new ResourceProcessor(srcDir, dstDir, ext)
        processor.start()
    }
}
