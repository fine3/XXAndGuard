package com.fine.ultimate.rehash

interface ResCopier {
    boolean onFileNeedCopy(File source, File dest);
}
