package com.fine.ultimate.rehash

class RehashImagesExt {
    String[] rehashIgnores = []
    Boolean debugEnable = false
    boolean enableNameGuard = false //change res names
    boolean enablePngMinify = false //minify pngs
    Boolean printLog = false
}