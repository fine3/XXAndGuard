package com.fine.ultimate.cleanaab

import com.android.build.gradle.api.ApplicationVariant
import com.android.zipflinger.ZipArchive
import org.gradle.api.Project

class CleanAabTask {

    Project mProject
    CleanAabExt mConfig
    ApplicationVariant mVariant

    CleanAabTask(Project project, ApplicationVariant variant) {
        mProject = project
        mConfig = project.cleanAab as CleanAabExt
        mVariant = variant
    }

    void start(File aabFile) {
        if (mVariant.buildType.debuggable && !mConfig.debugEnable) {
            printLog 'debug mode, nothing will happen!'
            return
        }
        ZipArchive archive = new ZipArchive(aabFile)
        archive.listEntries().each { entry ->
            if (entry.startsWith('BUNDLE-METADATA')) {
                archive.delete(entry)
                printLog "ignore file [${entry}]"
            }
        }
        archive.close()
    }

    private void printLog(String log) {
        if (mConfig.printLog) println log
    }
}