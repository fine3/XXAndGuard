package com.fine.ultimate.encode.asm

import com.hzy.xxteaw.De

interface IConstants {

    String STATIC_INIT = "<clinit>"

    String INIT = "<init>"
    String EMPTY_VOID_METHOD_DESC = "()V"
    String STRING_DESC = "Ljava/lang/String;"

    String FOG_CLASS_NAME = De.class.name.replace('.', '/')
    String FOG_DEC_METHOD = "de"
    String FOG_DEC_METHOD_DESC = "(Ljava/lang/String;)Ljava/lang/String;"

}
