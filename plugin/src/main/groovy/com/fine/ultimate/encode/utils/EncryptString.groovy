package com.fine.ultimate.encode.utils

/**
 * On 2020-10-10
 */
class EncryptString {

    static def random = new Random()

    static EncryptString getInstance() {
        return Holder.sInstance
    }

    private static class Holder {
        private static EncryptString sInstance = new EncryptString()
    }

    private Map<Object, StringEnc> mEnc = new HashMap<>()

    StringEnc enc(Object source) {
        StringEnc stringEnc = mEnc.get(source)
        if (stringEnc != null) {
            return stringEnc
        }
        stringEnc = FogUtils.encode(source, randomPwd())
        mEnc.put(source, stringEnc)
        return stringEnc
    }

    static String randomPwd() {
        StringBuilder sb = new StringBuilder()
        for (int i in 0..3) {
            sb.append((char) (random.nextInt(93) + 33))
        }
        return sb.toString()
    }
}
