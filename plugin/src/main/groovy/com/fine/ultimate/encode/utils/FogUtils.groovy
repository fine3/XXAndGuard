package com.fine.ultimate.encode.utils

import com.google.common.hash.Hashing
import com.hzy.xxteaw.De

import java.nio.charset.Charset


class FogUtils {

    static String md5(String s) {
        return Hashing.md5().newHasher().putString(s, Charset.forName('utf-8')).hash().toString()
    }

    static String uniqueName(File fileInput) {
        final String fileInputName = fileInput.getName()
        if (fileInput.isDirectory()) {
            return fileInputName
        }
        final String parentDirPath = fileInput.parentFile.absolutePath
        final String pathMD5 = md5(parentDirPath)
        final int extSepPos = fileInputName.lastIndexOf('.')
        final String fileInputNamePrefix =
                (extSepPos >= 0 ? fileInputName.substring(0, extSepPos) : fileInputName)
        return fileInputNamePrefix + '_' + pathMD5
    }

    static void closeSafely(Closeable c) {
        try {
            c.close()
        } catch (Throwable e) {
            e.printStackTrace()
        }
    }

    static void copyStream(InputStream is, OutputStream os) {
        byte[] buffer = new byte[8192]
        int c
        while ((c = is.read(buffer)) != -1) {
            os.write(buffer, 0, c)
        }
    }

    static StringEnc encode(Object src, String pwd) {
        if (src == null) {
            return StringEnc.failed()
        }
        if (!(src instanceof String)) {
            return StringEnc.failed()
        }
        if (src.trim().empty) {
            return StringEnc.failed()
        }
        try {
            return StringEnc.success(De.en(src, pwd), pwd)
        } catch (Throwable e) {
            e.printStackTrace()
            return StringEnc.failed()
        }
    }

}
