package com.fine.ultimate.encode


class StringEncodeExt {

    String[] basePackages = []
    Boolean debugEnable = false
    Boolean printLog = false
    Boolean ignoreJars = false

    boolean enabled() {
        basePackages != null && basePackages.length > 0
    }

    boolean log() {
        return printLog != null && printLog
    }

    boolean debugEnabled() {
        return enabled() && debugEnable != null && debugEnable
    }
}
