package com.fine.ultimate.junk

import org.gradle.api.tasks.Input

class JunkCodeExt {
    @Input
    String[] basePackages = []
    @Input
    int packageCount = 2
    @Input
    int activityCountPerPackage = 2
    @Input
    boolean excludeActivityJavaFile = false
    @Input
    int otherCountPerPackage = 2
    @Input
    int methodCountPerClass = 2
    @Input
    String resPrefix = ''
    @Input
    int imageCountLimit = 100
    @Input
    boolean debugEnable = false
}
