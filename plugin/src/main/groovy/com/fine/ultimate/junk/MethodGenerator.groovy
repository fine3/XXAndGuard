package com.fine.ultimate.junk

import com.squareup.javapoet.ClassName
import com.squareup.javapoet.MethodSpec

import javax.lang.model.element.Modifier
import java.math.MathContext

class MethodGenerator {

    AndroidJunkCodeTask mTask
    ClassName mLogClass

    MethodGenerator(AndroidJunkCodeTask task) {
        mLogClass = ClassName.get('android.util', "Log")
        mTask = task
    }

    private addOneStatement(MethodSpec.Builder mb) {
        mb.addModifiers(Modifier.PRIVATE)
        switch (random.nextInt(5)) {
            case 0:
                mb.beginControlFlow('')
                mb.addStatement("long now = \$T.currentTimeMillis()", System.class)
                        .beginControlFlow("if (\$T.currentTimeMillis() < now)", System.class)
                        .addStatement("\$T.i(\$S, \$S)", mLogClass, pickOneWord(), randomString())
                        .nextControlFlow("else if (\$T.currentTimeMillis() == now)", System.class)
                        .addStatement("\$T.out.println(\$S)", System.class, randomString())
                        .nextControlFlow("else")
                        .addStatement("\$T.out.println(\$S + now)", System.class, randomString())
                        .endControlFlow()
                mb.endControlFlow()
                break
            case 1:
                mb.beginControlFlow('')
                mb.addStatement("int total = 0")
                        .beginControlFlow("for (int i = 0; i < ${random.nextInt(100)}; i++)")
                        .addStatement("total += i")
                        .addStatement("\$T.i(\$S, \$S + i)", mLogClass, pickOneWord(), randomString())
                        .endControlFlow()
                mb.endControlFlow()
                break
            case 2:
                mb.beginControlFlow("try")
                        .addStatement("\$T.nanoTime()", System.class)
                        .addStatement("\$T.d(\$S, \$S)", mLogClass, pickOneWord(), randomString())
                mb.nextControlFlow("catch (\$T e)", Exception.class)
                        .addStatement("\$T.d(\$S, \$S)", mLogClass, pickOneWord(), randomString())
                mb.endControlFlow()
                break
            case 3:
                System.nanoTime().byteValue()
                mb.beginControlFlow("try")
                        .addStatement("\$T.nanoTime()", System.class)
                        .addStatement("\$T.out.println(\$S)", System.class, randomString())
                mb.nextControlFlow("catch (\$T e)", Exception.class)
                        .addStatement("\$T.d(\$S, \$S)", mLogClass, pickOneWord(), randomString())
                mb.nextControlFlow("finally")
                        .addStatement("\$T.d(\$S, \$S)", mLogClass, pickOneWord(), randomString())
                mb.endControlFlow()
                break
            case 4:
                mb.beginControlFlow("try")
                        .addStatement("\$T.randomUUID()", UUID.class)
                        .addStatement("\$T.d(\$S, \$S)", mLogClass, pickOneWord(), randomString())
                mb.nextControlFlow("catch (\$T e)", Exception.class)
                        .addStatement("\$T.d(\$S, \$S)", mLogClass, pickOneWord(), randomString())
                mb.endControlFlow()
                break
            default:
                mb.beginControlFlow("try")
                        .addStatement("\$T.DECIMAL32.toString()", MathContext.class)
                        .addStatement("\$T.reverseOrder()", Collections.class)
                        .addStatement("\$T.d(\$S, \$S)", mLogClass, pickOneWord(), randomString())
                mb.nextControlFlow("catch (\$T e)", Exception.class)
                        .addStatement("\$T.d(\$S, \$S)", mLogClass, pickOneWord(), randomString())
                mb.endControlFlow()
        }
    }

    MethodSpec randomMethod(String methodName, String nextMethod) {
        def mb = MethodSpec.methodBuilder(methodName)
        def flowCount = random.nextInt(4) + 1
        for (i in 0..flowCount) {
            addOneStatement(mb)
        }
        if (nextMethod != null) {
            mb.addStatement("${nextMethod}()")
        }
        return mb.build()
    }

    static Random getRandom() {
        return AndroidJunkCodeTask.random
    }

    String randomString() {
        return mTask.randomString()
    }

    String pickOneWord() {
        return mTask.pickOneWord()
    }

    static MethodSpec generateOnResume(String nextMethod) {
        def mb = MethodSpec.methodBuilder("onResume")
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PROTECTED)
                .addStatement("super.onResume()")
        if (nextMethod != null) {
            mb.addStatement("${nextMethod}()")
        }
        return mb.build()
    }
}
