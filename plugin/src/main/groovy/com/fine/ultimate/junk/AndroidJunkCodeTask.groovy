package com.fine.ultimate.junk

import com.google.common.io.CharStreams
import com.squareup.javapoet.ClassName
import com.squareup.javapoet.JavaFile
import com.squareup.javapoet.MethodSpec
import com.squareup.javapoet.TypeSpec
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Nested
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

import javax.lang.model.element.Modifier
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path

class AndroidJunkCodeTask extends DefaultTask {

    static def random = new Random()
    static CHARS_ALPHA = "abcdefghijklmnopqrstuvwxyz"
    static abcLength = CHARS_ALPHA.length()

    @Nested
    JunkCodeExt config
    @Input
    String namespace
    @OutputDirectory
    File outDir

    private String mLayoutTemplate = ''
    private String mDrawableTemplate = ''
    private String mManifestTemplate = ''

    private List<String> mActivityList = new LinkedList<>()
    private List<String> mStringList = new LinkedList<>()
    private List<String> mColorList = new LinkedList<>()
    private List<String> mDimenList = new LinkedList<>()
    private List<String> mEnglishWords
    private List<String> mDrawableList = new LinkedList<>()
    private MethodGenerator methodGen = new MethodGenerator(this)

    @TaskAction
    void generateJunkCode() {
        if (outDir.exists()) {
            outDir.deleteDir()
        }
        mActivityList.clear()
        mStringList.clear()
        mColorList.clear()
        mDimenList.clear()
        loadTemplates()
        //通过成类
        generateClasses()
        //生成资源
        generateRes()
    }

    void loadTemplates() {
        mLayoutTemplate = loadStringResource("template/temp_layout.xml")
        mDrawableTemplate = loadStringResource("template/temp_drawable.xml")
        mManifestTemplate = loadStringResource("template/temp_manifest.xml")
        mEnglishWords = loadStringLineResource("template/temp_alpha_words.txt")
    }

    List<String> loadStringLineResource(String path) {
        def is = getClass().getClassLoader().getResourceAsStream(path)
        return new InputStreamReader(is, Charset.forName("UTF-8")).readLines()
    }

    String loadStringResource(String path) {
        def is = getClass().getClassLoader().getResourceAsStream(path)
        return CharStreams.toString(new InputStreamReader(is, StandardCharsets.UTF_8))
    }

    void generateClasses() {
        for (String basePackage : config.basePackages) {
            for (int i = 0; i < config.packageCount; i++) {
                String packageName
                if (basePackage.isEmpty()) {
                    packageName = generateName(i)
                } else {
                    packageName = basePackage + "." + generateName(i)
                }
                //生成Activity
                for (int j = 0; j < config.activityCountPerPackage; j++) {
                    def activityPreName = generateName(j)
                    generateActivity(packageName, activityPreName, j)
                }
                //生成其它类
                for (int j = 0; j < config.otherCountPerPackage; j++) {
                    def className = generateName(j).capitalize()
                    def typeBuilder = TypeSpec.classBuilder(className)
                    typeBuilder.addModifiers(Modifier.PUBLIC)
                    for (int k = 0; k < config.methodCountPerClass; k++) {
                        typeBuilder.addMethod(methodGen.randomMethod(generateName(k), null))
                    }
                    def javaFile = JavaFile.builder(packageName, typeBuilder.build()).build()
                    writeJavaFile(javaFile)
                }
            }
        }
        generateManifest()
    }

    void generateActivity(String packageName, String activityPreName, int index) {
        def className = activityPreName.capitalize() + "Activity"
        def layoutName = "${config.resPrefix.toLowerCase()}${packageName.replace(".", "_")}_activity_${activityPreName}"
        def layoutIds = generateLayout(layoutName, index)
        if (!config.excludeActivityJavaFile) {
            def typeBuilder = TypeSpec.classBuilder(className)
            typeBuilder.superclass(ClassName.get("android.app", "Activity"))
            typeBuilder.addModifiers(Modifier.PUBLIC)
            typeBuilder.addMethod(generateOnCreate(layoutName, layoutIds, index))
            def methodNames = new LinkedList<String>()
            for (int j = 0; j < config.methodCountPerClass; j++) {
                methodNames.add(generateName(j))
                def curName = methodNames.last
                def lastName = methodNames.size() < 2 ? null : methodNames[methodNames.size() - 2]
                typeBuilder.addMethod(methodGen.randomMethod(curName, lastName))
            }
            def curName = methodNames.size() > 0 ? methodNames.last : null
            typeBuilder.addMethod(methodGen.generateOnResume(curName))
            def javaFile = JavaFile.builder(packageName, typeBuilder.build()).build()
            writeJavaFile(javaFile)
        }
        mActivityList.add(packageName + "." + className)
    }

    MethodSpec generateOnCreate(String layoutName, List<String> layoutIds, int index) {
        def classBundle = ClassName.get("android.os", "Bundle")
        def classR = ClassName.get(namespace, "R")
        def classImgView = ClassName.get("android.widget", "ImageView")
        def classTextView = ClassName.get("android.widget", "TextView")
        def varNames = [randomLayoutId(), randomLayoutId()]
        def strRes = newResName(index)
        mStringList.add(strRes)
        def drawableId = generateDrawable(index)
        return MethodSpec.methodBuilder("onCreate")
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PROTECTED)
                .addParameter(classBundle, "savedInstanceState")
                .addStatement("super.onCreate(savedInstanceState)")
                .addStatement("setContentView(\$T.layout.${layoutName})", classR)
                .addStatement("\$T ${varNames[0]} = findViewById(\$T.id.${layoutIds[0]})", classTextView, classR)
                .addStatement("${varNames[0]}.setText(\$T.string.${strRes})", classR)
                .addStatement("\$T ${varNames[1]} = findViewById(\$T.id.${layoutIds[1]})", classImgView, classR)
                .addStatement("${varNames[1]}.setImageResource(\$T.drawable.${drawableId})", classR)
                .build()
    }

    void generateRes() {
        generateDimenFile()
        generateStringsFile()
        generateColorFile()
    }

    String newResName(int i) {
        return "${config.resPrefix.toLowerCase()}${pickOneWord()}_${generateName(i)}"
    }

    String generateDrawable(index) {
        def total = mDrawableList.size()
        if (total >= config.imageCountLimit) {
            return mDrawableList.get(random.nextInt(total))
        }
        def drawableName = newResName(index)
        mDrawableList.add(drawableName)
        def drawableFile = new File(outDir, "res/drawable/${drawableName}.xml")
        def drawableStr = mDrawableTemplate
                .replace("@FILL_COLOR1", randomColor())
                .replace("@FILL_COLOR2", randomColor())
                .replace("@FILL_COLOR3", randomColor())
                .replace("@FILL_COLOR4", randomColor())
        writeStringToFile(drawableFile, drawableStr)
        return drawableName
    }

    String randomLayoutId() {
        return "id_${pickOneWord()}_${generateId()}"
    }

    List<String> generateLayout(String layoutName, int index) {
        def layoutFile = new File(outDir, "res/layout/${layoutName}.xml")
        def drawableId = generateDrawable(index)
        def paddingRes = newResName(index)
        mDimenList.add(paddingRes)
        def strRes = newResName(index)
        mStringList.add(strRes)
        def colors = [newResName(index), newResName(index)]
        mColorList.addAll(colors)
        def layoutIds = [randomLayoutId(), randomLayoutId()]
        def layoutStr = mLayoutTemplate
                .replace('@BG_PADDING', paddingRes)
                .replace('@TXT_COLOR', colors[0])
                .replace('@BG_COLOR', colors[1])
                .replace("@ID_TXT", layoutIds[0])
                .replace("@ID_STR", strRes)
                .replace("@ID_IMG", layoutIds[1])
                .replace("@ID_DRAW", drawableId)
        writeStringToFile(layoutFile, layoutStr)
        return layoutIds
    }


    /**
     * 生成Manifest*/
    void generateManifest() {
        def manifestFile = new File(outDir, "AndroidManifest.xml")
        StringBuilder sb = new StringBuilder()
        for (i in 0..<mActivityList.size()) {
            sb.append("        <activity android:name=\"${mActivityList.get(i)}\"/>\n")
        }
        writeStringToFile(manifestFile, mManifestTemplate.replace("activity_list", sb.toString()))
    }

    void generateStringsFile() {
        def stringFile = new File(outDir, "res/values/strings.xml")
        StringBuilder sb = new StringBuilder()
        sb.append("<resources>\n")
        for (i in mStringList) {
            sb.append("    <string name=\"${i}\">${randomString()}</string>\n")
        }
        sb.append("</resources>\n")
        writeStringToFile(stringFile, sb.toString())
    }

    void generateColorFile() {
        def stringFile = new File(outDir, "res/values/colors.xml")
        StringBuilder sb = new StringBuilder()
        sb.append("<resources>\n")
        for (i in mColorList) {
            sb.append("    <color name=\"${i}\">${randomColor()}</color>\n")
        }
        sb.append("</resources>\n")
        writeStringToFile(stringFile, sb.toString())
    }

    void generateDimenFile() {
        def stringFile = new File(outDir, "res/values/dimens.xml")
        StringBuilder sb = new StringBuilder()
        sb.append("<resources>\n")
        for (i in mDimenList) {
            sb.append("    <dimen name=\"${i}\">${random.nextInt(80)}dp</dimen>\n")
        }
        sb.append("</resources>\n")
        writeStringToFile(stringFile, sb.toString())
    }

    private void writeJavaFile(JavaFile javaFile) {
        def outputDirectory = outDir.toPath().resolve("java")
        if (!javaFile.packageName.isEmpty()) {
            for (String packageComponent : javaFile.packageName.split("\\.")) {
                outputDirectory = outputDirectory.resolve(packageComponent);
            }
            Files.createDirectories(outputDirectory)
        }
        Path outputPath = outputDirectory.resolve(javaFile.typeSpec.name + ".java");
        writeStringToFile(outputPath.toFile(), javaFile.toString())
    }

    private static void writeStringToFile(File file, String data) {
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs()
        }
        FileWriter writer
        try {
            writer = new FileWriter(file, Charset.forName("UTF-8"))
            writer.write(data)
        } catch (Exception e) {
            e.printStackTrace()
        } finally {
            if (writer != null) {
                writer.close()
            }
        }
    }

    String pickOneWord() {
        mEnglishWords.get(random.nextInt(mEnglishWords.size()))
    }

    static String generateName(int index) {
        def sb = new StringBuilder()
        for (i in 0..4) {
            sb.append(CHARS_ALPHA.charAt(random.nextInt(abcLength)))
        }
        int temp = index
        while (temp >= 0) {
            sb.append(CHARS_ALPHA.charAt(temp % abcLength))
            temp = temp / abcLength
            if (temp == 0) {
                temp = -1
            }
        }
        sb.append(index.toString())
        return sb.toString()
    }

    String randomString() {
        int words = random.nextInt(14) + 2
        def sb = new StringBuilder()
        for (i in 0..words) {
            def wd = mEnglishWords.get(random.nextInt(mEnglishWords.size()))
            if (i == 0) {
                if (words % 3 != 0) {
                    wd = wd.capitalize()
                }
            } else {
                sb.append(" ")
            }
            sb.append(wd)
        }
        return sb.toString()
    }

    /**
     * 生成颜色代码
     * @return
     */
    static String randomColor() {
        return String.format("#%06X", random.nextInt(0xFFFFFF))
    }
    /**
     * 生成id代码
     * @return
     */
    static String generateId() {
        def sb = new StringBuilder()
        for (i in 0..5) {
            sb.append(CHARS_ALPHA.charAt(random.nextInt(abcLength)))
        }
        return sb.toString()
    }
}