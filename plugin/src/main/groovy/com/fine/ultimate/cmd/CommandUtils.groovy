package com.fine.ultimate.cmd


import com.fine.ultimate.rehash.OSDetector
import org.apache.commons.io.FileUtils

class CommandUtils {

    static void minifyPNGWithCommand(File cmdFile, String src, String dst) {
        def pngCmd = ensurePNGQuantCMD(cmdFile)
        ProcessBuilder pb = new ProcessBuilder(pngCmd, '--force', '--output', dst, src)
        Process pro = pb.start()
        InputStreamReader ir = new InputStreamReader(pro.getInputStream())
        LineNumberReader input = new LineNumberReader(ir)
        while (input.readLine() != null) {
        }
        pro.waitFor()
        pro.destroy()
    }

    static void compressWithCommand(File cmdFile, String dst, String src, String level) {
        def zipCmd = ensure7zCMD(cmdFile)
        ProcessBuilder pb = new ProcessBuilder(zipCmd, 'a', '-tzip', dst, src, level)
        Process pro = pb.start()
        InputStreamReader ir = new InputStreamReader(pro.getInputStream())
        LineNumberReader input = new LineNumberReader(ir)
        while (input.readLine() != null) {
        }
        pro.waitFor()
        pro.destroy()
    }

    private static String ensure7zCMD(File cmdFile) {
        return ensureSomeCMD(cmdFile, "p7zip/SevenZip-${OSDetector.archExt}")
    }

    private static String ensurePNGQuantCMD(File cmdFile) {
        return ensureSomeCMD(cmdFile, "pngquant/pngquant-${OSDetector.archExt}")
    }

    private static String ensureSomeCMD(File cmdFile, String resPath) {
        if (cmdFile.exists()) {
            cmdFile.setExecutable(true, false)
            return cmdFile.absolutePath
        }
        def is = CommandUtils.class.getClassLoader().getResourceAsStream(resPath)
        FileUtils.copyToFile(is, cmdFile)
        cmdFile.setExecutable(true, false)
        return cmdFile.absolutePath
    }
}
