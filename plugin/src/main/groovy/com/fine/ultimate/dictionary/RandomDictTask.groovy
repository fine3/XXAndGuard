package com.fine.ultimate.dictionary

import com.google.common.io.Files
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets

class RandomDictTask extends DefaultTask {

    public File proguardFile
    public File dictionaryFile

    private RandomDictExt mConfig

    @TaskAction
    void onAction() {
        mConfig = project.extensions.findByType(RandomDictExt)
        deleteFile(proguardFile)
        deleteFile(dictionaryFile)
        Files.createParentDirs(proguardFile)
        List<String> srcDict = new LinkedList<>()
        if (!mConfig.srcDictName.isBlank()) {
            def srcDictFile = new File(mConfig.srcDictName)
            if (srcDictFile.exists()) {
                def is = new FileInputStream(srcDictFile)
                srcDict = new InputStreamReader(is, Charset.forName("UTF-8")).readLines()
            }
        }
        if (srcDict.empty) {
            def is = getClass().getClassLoader().getResourceAsStream("template/temp_complex_dict.txt")
            srcDict = new InputStreamReader(is, Charset.forName("UTF-8")).readLines()
        }
        StringBuilder sb = new StringBuilder()
        sb.append("-obfuscationdictionary ").append(dictionaryFile.name).append('\n')
        sb.append("-classobfuscationdictionary ").append(dictionaryFile.name).append('\n')
        sb.append("-packageobfuscationdictionary ").append(dictionaryFile.name).append('\n')
        printLog 'creating new dictionary!'
        Files.asCharSink(proguardFile, StandardCharsets.UTF_8).write(sb.toString())
        Collections.shuffle(srcDict)
        sb = new StringBuilder()
        srcDict.each { sb.append(it).append('\n') }
        Files.asCharSink(dictionaryFile, StandardCharsets.UTF_8).write(sb.toString())
    }

    private void printLog(String log) {
        if (mConfig.printLog)
            println log
    }

    private static void deleteFile(File file) {
        if (file.exists()) {
            if (!file.delete()) {
                throw new IllegalStateException("Fail to delete file ${file}")
            }
        }
    }
}