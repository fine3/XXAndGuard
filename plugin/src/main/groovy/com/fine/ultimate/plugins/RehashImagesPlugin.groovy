package com.fine.ultimate.plugins


import com.android.build.gradle.AppExtension
import com.android.build.gradle.api.ApplicationVariant
import com.android.build.gradle.internal.res.LinkApplicationAndroidResourcesTask
import com.fine.ultimate.rehash.RehashImageTask
import com.fine.ultimate.rehash.RehashImagesExt
import org.gradle.api.Plugin
import org.gradle.api.Project

class RehashImagesPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        def android = project.extensions.getByType(AppExtension.class)
        if (!android || !android.hasProperty('applicationVariants')) {
            throw IllegalArgumentException("must apply this plugin after 'com.android.application'")
        }
        project.extensions.create('rehashImages', RehashImagesExt)
        project.afterEvaluate {
            android.applicationVariants.all { variant ->
                handleVariant(project, variant)
            }
        }
    }

    static void handleVariant(Project project, ApplicationVariant variant) {
        def variantName = variant.name.capitalize()
        def processResources = project.tasks.getByName("process${variantName}Resources")
        processResources.doLast {
            def resourcesTask = it as LinkApplicationAndroidResourcesTask
            def resDir = resourcesTask.resPackageOutputFolder
            resDir.asFileTree.each { file ->
                if (file.name.endsWith('.ap_')) {
                    new RehashImageTask(project, variant).start(file)
                }
            }
        }
    }
}