package com.fine.ultimate.plugins

import com.android.build.gradle.AppExtension
import com.fine.ultimate.encode.EncodeTransform
import com.fine.ultimate.encode.StringEncodeExt
import org.gradle.api.Plugin
import org.gradle.api.Project

class StringEncodePlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        def android = project.extensions.getByType(AppExtension)
        if (!android || !android.hasProperty('applicationVariants')) {
            throw IllegalArgumentException("must apply this plugin after 'com.android.application'")
        }
        project.dependencies.add("implementation", 'com.gitlab.fine3:XXTeaWrapJni:1.0.3')
        project.extensions.create("stringEncode", StringEncodeExt)
        android.registerTransform(new EncodeTransform(project))
    }
}