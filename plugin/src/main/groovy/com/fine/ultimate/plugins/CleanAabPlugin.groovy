package com.fine.ultimate.plugins

import com.android.build.gradle.AppExtension
import com.android.build.gradle.api.ApplicationVariant
import com.android.build.gradle.internal.tasks.PackageBundleTask
import com.fine.ultimate.cleanaab.CleanAabExt
import com.fine.ultimate.cleanaab.CleanAabTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class CleanAabPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        def android = project.extensions.getByType(AppExtension.class)
        if (!android || !android.hasProperty('applicationVariants')) {
            throw IllegalArgumentException("must apply this plugin after 'com.android.application'")
        }
        project.extensions.create('cleanAab', CleanAabExt)
        project.afterEvaluate {
            android.applicationVariants.all { variant ->
                handleVariant(project, variant)
            }
        }
    }

    static void handleVariant(Project project, ApplicationVariant variant) {
        def variantName = variant.name.capitalize()
        def pkgTask = project.tasks.getByName("package${variantName}Bundle") as PackageBundleTask
        pkgTask.doLast {
            def aabFile = pkgTask.bundleFile.asFile.get()
            new CleanAabTask(project, variant).start(aabFile)
        }
    }
}
