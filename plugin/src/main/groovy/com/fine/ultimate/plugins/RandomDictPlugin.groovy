package com.fine.ultimate.plugins

import com.android.build.gradle.AppExtension
import com.android.build.gradle.api.ApplicationVariant
import com.android.build.gradle.internal.tasks.ProguardConfigurableTask
import com.fine.ultimate.dictionary.RandomDictExt
import com.fine.ultimate.dictionary.RandomDictTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class RandomDictPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        def android = project.extensions.getByType(AppExtension.class)
        if (!android || !android.hasProperty('applicationVariants')) {
            throw IllegalArgumentException("must apply this plugin after 'com.android.application'")
        }
        project.extensions.create('randomDict', RandomDictExt)
        project.afterEvaluate {
            android.applicationVariants.all { variant ->
                handleVariant(project, variant)
            }
        }
    }

    private static void handleVariant(Project project, ApplicationVariant variant) {
        String variantName = variant.name.capitalize()
        def task = project.tasks.findByName("minify${variantName}WithProguard")
        installTask(project, variant, task)
        task = project.tasks.findByName("minify${variantName}WithR8")
        installTask(project, variant, task)
    }

    private static void installTask(Project project, ApplicationVariant variant, ProguardConfigurableTask task) {
        if (task == null) {
            return
        }
        File proguardFile = new File(project.buildDir, "intermediates/proguard-dictionary/${variant.dirName}/proguard-gen.txt")
        File dictionaryFile = new File(project.buildDir, "intermediates/proguard-dictionary/${variant.dirName}/dictionary-gen.txt")
        def files = project.files(proguardFile)
        task.configurationFiles.from(files)
        def dicTask = project.tasks.create("random${variant.name.capitalize()}Dict", RandomDictTask)
        dicTask.proguardFile = proguardFile
        dicTask.dictionaryFile = dictionaryFile
        task.dependsOn(dicTask)
    }
}
