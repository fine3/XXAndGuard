package com.fine.ultimate.plugins

import com.android.build.gradle.AppExtension
import com.android.build.gradle.api.ApplicationVariant
import com.android.build.gradle.internal.res.LinkAndroidResForBundleTask
import com.fine.ultimate.aab.RehashAabExt
import com.fine.ultimate.aab.RehashAabTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class RehashAabPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        def android = project.extensions.getByType(AppExtension.class)
        if (!android || !android.hasProperty('applicationVariants')) {
            throw IllegalArgumentException("must apply this plugin after 'com.android.application'")
        }
        project.extensions.create('rehashAab', RehashAabExt)
        project.afterEvaluate {
            android.applicationVariants.all { variant ->
                handleVariant(project, variant)
            }
        }
    }

    static void handleVariant(Project project, ApplicationVariant variant) {
        def variantName = variant.name.capitalize()
        def bundleResources = project.tasks.getByName("bundle${variantName}Resources")
        bundleResources.doLast {
            def linkTask = it as LinkAndroidResForBundleTask
            def apFile = linkTask.bundledResFile.asFile.get()
            new RehashAabTask(project, variant).start(apFile)
        }
    }
}