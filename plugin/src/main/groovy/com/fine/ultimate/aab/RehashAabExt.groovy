package com.fine.ultimate.aab

class RehashAabExt {
    Boolean debugEnable = false
    String[] rehashIgnores = []
    Boolean enableNameGuard = false
    Boolean enablePngMinify = false
    Boolean printLog = false
}