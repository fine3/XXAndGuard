package com.fine.ultimate.aab

import com.android.aapt.Resources
import com.android.bundle.Config
import com.android.tools.build.libraries.metadata.AppDependencies
import com.google.common.io.Files
import com.google.protobuf.util.JsonFormat

import java.nio.charset.StandardCharsets

class AabDemoApp {

    static void main(String[] args) {
//        parseDependencies()
//        parseBundleConfig()
        parseResourcesConfig()
//        parseAssetsConfig()
//        parseNativeConfig()
    }

    static void parseNativeConfig() {
        File srcDir = new File('repo', 'temp')
        File dstDir = new File('repo', 'resOut')
        File pbFile = new File(srcDir, 'native.pb')
        File depFile = new File(dstDir, 'native.json')
        dstDir.mkdirs()
        def config = com.android.bundle.Files.NativeLibraries.parseFrom(new FileInputStream(pbFile))
        def jString = JsonFormat.printer().print(config)
        Files.asCharSink(depFile, StandardCharsets.UTF_8).write(jString)
    }

    static void parseAssetsConfig() {
        File srcDir = new File('repo', 'temp')
        File dstDir = new File('repo', 'resOut')
        File pbFile = new File(srcDir, 'assets.pb')
        File depFile = new File(dstDir, 'assets.json')
        dstDir.mkdirs()
        def config = com.android.bundle.Files.Assets.parseFrom(new FileInputStream(pbFile))
        def jString = JsonFormat.printer().print(config)
        Files.asCharSink(depFile, StandardCharsets.UTF_8).write(jString)
    }

    static void parseResourcesConfig() {
        File srcDir = new File('repo', 'temp')
        File dstDir = new File('repo', 'resOut')
        File pbFile = new File(srcDir, 'resources.pb')
        File depFile = new File(dstDir, 'resources.json')
        dstDir.mkdirs()
        def config = Resources.ResourceTable.parseFrom(new FileInputStream(pbFile))
        def jString = JsonFormat.printer().print(config)
        Files.asCharSink(depFile, StandardCharsets.UTF_8).write(jString)
    }

    static void parseBundleConfig() {
        File srcDir = new File('repo', 'temp')
        File dstDir = new File('repo', 'resOut')
        File pbFile = new File(srcDir, 'BundleConfig.pb')
        File depFile = new File(dstDir, 'BundleConfig.json')
        dstDir.mkdirs()
        def config = Config.BundleConfig.parseFrom(new FileInputStream(pbFile))
        def jString = JsonFormat.printer().print(config)
        Files.asCharSink(depFile, StandardCharsets.UTF_8).write(jString)
    }

    static void parseDependencies() {
        File srcDir = new File('repo', 'temp')
        File dstDir = new File('repo', 'resOut')
        File pbFile = new File(srcDir, 'dependencies.pb')
        File depFile = new File(dstDir, 'dependencies.json')
        dstDir.mkdirs()
        def deps = AppDependencies.parseFrom(new FileInputStream(pbFile))
        def jString = JsonFormat.printer().print(deps)
        Files.asCharSink(depFile, StandardCharsets.UTF_8).write(jString)
    }

    static void parseAabPB() {
        File srcDir = new File('repo', 'temp')
        File dstDir = new File('repo', 'resOut')
        def ext = new RehashAabExt()
        ext.enableNameGuard = true
        ext.enablePngMinify = true
        AabSubProcessor processor = new AabSubProcessor(srcDir, dstDir, ext)
        processor.start()
    }
}
