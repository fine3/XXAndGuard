package com.fine.ultimate.aab

import com.fine.ultimate.cmd.CommandUtils
import com.fine.ultimate.rehash.ResCopier
import org.apache.commons.io.FileUtils

class AabSubProcessor implements ResCopier {

    private final File mSrcDir
    private final File mDstDir
    private final File mPngProcFile
    private RehashAabExt mConfig

    AabSubProcessor(File srcDir, File dstDir, RehashAabExt ext) {
        mSrcDir = srcDir
        mDstDir = dstDir
        mConfig = ext
        mPngProcFile = new File(srcDir.parent, 'pngquant')
    }

    void start() {
        if (mDstDir.exists()) {
            FileUtils.deleteDirectory(mDstDir)
        }
        mDstDir.mkdirs()
        if (mConfig.enableNameGuard) {
            PbResProcessor processor = new PbResProcessor(mSrcDir, mDstDir)
            processor.setCopier(this)
            processor.parsePB()
            copyOtherFiles()
        } else {
            FileUtils.copyDirectory(mSrcDir, mDstDir)
        }
    }

    private void copyOtherFiles() throws Exception {
        if (!(mSrcDir.exists() && mSrcDir.isDirectory())) {
            throw new Exception("src path error!!")
        }
        File[] subs = mSrcDir.listFiles()
        if (subs == null) {
            return
        }
        subs.each {
            String name = it.name.toLowerCase()
            if (name != 'resources.pb' && name != 'res') {
                FileUtils.copyToDirectory(it, mDstDir)
            }
        }
    }

    @Override
    boolean onFileNeedCopy(File source, File dest) {
        def srcName = source.name.toLowerCase()
        if (needMinify(srcName)) {
            dest.parentFile.mkdirs()
            CommandUtils.minifyPNGWithCommand(mPngProcFile, source.path, dest.path)
            if (mConfig.printLog) {
                println "minify png: ${source.name}(${getReadableSize(source)}) -> ${dest.name}(${getReadableSize(dest)})"
            }
        } else {
            FileUtils.copyFile(source, dest, false)
        }
        return true
    }

    private static String getReadableSize(File file) {
        return FileUtils.byteCountToDisplaySize(file.size())
    }

    private boolean needMinify(String srcName) {
        if (!mConfig.enablePngMinify) {
            return false
        }
        if (srcName.endsWith('.png') && !srcName.endsWith('.9.png')) {
            return true
        }
        return false
    }
}
