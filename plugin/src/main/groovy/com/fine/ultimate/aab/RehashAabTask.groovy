package com.fine.ultimate.aab

import com.android.build.gradle.api.ApplicationVariant
import org.apache.commons.io.FileUtils
import org.gradle.api.Project

class RehashAabTask {

    Project mProject
    RehashAabExt mConfig
    ApplicationVariant mVariant
    AabZipOperator mFileOp

    RehashAabTask(Project project, ApplicationVariant variant) {
        mProject = project
        mConfig = project.rehashAab as RehashAabExt
        mVariant = variant
    }

    void start(File apFile) {
        if (mVariant.buildType.debuggable && !mConfig.debugEnable) {
            return
        }
        def tempDir = new File(apFile.parent, '_temp/')
        def srcDir = new File(tempDir, 'src')
        def dstDir = new File(tempDir, 'dst')
        def tempAP = new File(tempDir, 'out.ap_')

        mFileOp = new AabZipOperator(mProject, mVariant, 'rehash-aab-map')
        printLog "unpacking [${apFile.name}]"
        mFileOp.unpackAPFile(apFile.path, srcDir.path)
        AabSubProcessor processor = new AabSubProcessor(srcDir, dstDir, mConfig)
        processor.start()
        printLog "repacking [${apFile.name}]"
        mFileOp.repackAPFile(dstDir, tempAP.path)
        def apPath = apFile.path
        FileUtils.delete(apFile)
        FileUtils.moveFile(tempAP, new File(apPath))
    }

    private void printLog(String log) {
        if (mConfig.printLog)
            println log
    }
}