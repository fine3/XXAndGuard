package com.fine.ultimate.aab

import com.android.aapt.Resources
import com.fine.ultimate.rehash.ResCopier
import com.fine.ultimate.rehash.UniqueGenerator
import com.google.protobuf.util.JsonFormat

class PbResProcessor {

    private final File mSrcDir
    private final File mDstDir
    private final File mResDir
    private final File mPBFile
    private ResCopier mResCopier

    PbResProcessor(File srcDir, File dstDir) throws Exception {
        mSrcDir = srcDir
        mDstDir = dstDir
        mResDir = new File(srcDir, "res")
        if (!(mResDir.exists() && mResDir.isDirectory())) {
            throw new Exception("res not exists!")
        }
        mPBFile = new File(srcDir, "resources.pb")
        if (!(mPBFile.exists() && mPBFile.isFile())) {
            throw new Exception("resources.pb not exists!")
        }
    }

    void setCopier(ResCopier copier) {
        mResCopier = copier
    }

    void parsePB() throws Exception {
        def is = new FileInputStream(mPBFile)
        def resTable = Resources.ResourceTable.parseFrom(is)
        is.close()
        def jString = JsonFormat.printer()
                .omittingInsignificantWhitespace()
                .print(resTable)
        def uniGenerator = new UniqueGenerator()
        def fileCount = 0
        mResDir.listFiles().each { sub ->
            if (sub.isDirectory()) {
                fileCount += sub.list().size()
            } else {
                fileCount++
            }
        }
        uniGenerator.generateNames(fileCount + 100)
        mResDir.listFiles().each { sub ->
            if (sub.isDirectory()) {
                def subName = "res/${sub.name}/"
                sub.listFiles().each { srcFile ->
                    def resPath = "${subName}${srcFile.name}"
                    def resExt = getLongExtension(srcFile)
                    def genName = uniGenerator.fetch()
                    def destRes = "${subName}${genName}${resExt}"
                    jString = jString.replace("\"path\":\"${resPath}\"", "\"path\":\"${destRes}\"")
                    def destFile = new File(mDstDir, destRes.replace('/', File.separator))
                    if (!destFile.exists() && mResCopier != null) {
                        mResCopier.onFileNeedCopy(srcFile, destFile)
                    }
                }
            }
        }
        def nb = Resources.ResourceTable.newBuilder()
        JsonFormat.parser().merge(jString, nb)
        def tableNew = nb.build()
        File pbOut = new File(mDstDir, 'resources.pb')
        def os = new FileOutputStream(pbOut)
        tableNew.writeTo(os)
        os.flush()
        os.close()
    }

    private static String getLongExtension(File file) {
        def raw = file.name
        def dot1 = raw.indexOf('.')
        if (dot1 != -1) {
            return raw.substring(dot1)
        }
        return ''
    }
}
